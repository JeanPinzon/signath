import React from 'react'
import ReactDOM from 'react-dom'

import DesktopApp from './components/DesktopApp'
import * as serviceWorker from './serviceWorker'

import './assets/styles/index.scss'


ReactDOM.render(<DesktopApp />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
