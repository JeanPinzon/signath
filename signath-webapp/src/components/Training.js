import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

import Button from './common/Button'
import Loader from './common/Loader'

import '../assets/styles/Training.scss'


class Training extends Component {

  constructor(props) {
    super(props)

    this.renderingByStage = {
      noSignal: () => {
        return this.renderSignalLangTraining(
          this.noSignal.value,
          this.noSignal.onFinish,
          this.noSignal.name,
          this.noSignal.explanation
        )
      }
    }

    this.numberSignals = [0, 1, 2]

    if (props.advancedMode) {
      this.numberSignals = [0 ,1, 2, 3, 4, 5, 6, 7, 8, 9]
    }

    this.numberSignals.forEach((i) => {
      this.renderingByStage[`${i}`] = () => {
        let nextStageFunction = () => this.setState({ stage: `${i + 1}` })

        if (i === this.numberSignals[this.numberSignals.length - 1]) {
          nextStageFunction = () => this.setState({ trainingFinished: true })
        }

        return this.renderSignalLangTraining(i.toString(), nextStageFunction)
      }
    })
  }

  state = {
    stage: null,
    trainingStarted: false,
    trainingSignal: false,
    trainedSignals: {}
  }

  noSignal = {
    value: 'noSignal',
    name: 'Ambiente',
    explanation: 'Clique em "Treinar" sem fazer nenhum sinal para a câmera.',
    onFinish: () => this.setState({ stage: '0' })
  }

  isNoSignalStage = () => this.state.stage === this.noSignal.value

  isNumberStage = () => this.numberSignals.map(n => n.toString()).includes(this.state.stage)

  shouldTrainNumbersClassifier = () => this.isNoSignalStage() || this.isNumberStage()

  classifySignal = (signal, classifier, classifierName) => {
    console.log(`[Signath] Starting to train ${classifierName} with signal ${signal}`)

    const train = (onFinish, count = 0) => {
      if (count >= 15) {
        console.log(`[Signath] Finish training ${classifierName} with signal ${signal} -> ${count}`)
        return onFinish()
      }

      console.log(`[Signath] Training ${classifierName} with signal ${signal} -> ${count}`)

      if (count === 7) {
        console.log(`[Signath] Setting image for signal ${signal}`)
        this.props.setImageForSignal(signal, this.props.getMaskedImage())
      }

      const features = this.props.getFeatureExtractor().infer(this.props.getMaskedCanvas())
      classifier.addExample(features, signal)

      setTimeout(() => train(onFinish, ++count), 100)
    }

    return new Promise((resolve) => train(resolve))
  }

  trainNumbersClassifier = () => this.classifySignal(
    this.state.stage,
    this.props.getNumbersClassifier(),
    'NumbersClassifier'
  )

  trainSignal = () => {
    this.setState({ trainingSignal: true }, () => {

      const trainPromises = []

      if (this.shouldTrainNumbersClassifier()) {
        trainPromises.push(this.trainNumbersClassifier())
      }

      Promise.all(trainPromises).then(() => {
        this.setState((prevState) => ({
          trainingSignal: false,
          trainedSignals: {
            ...prevState.trainedSignals,
            [prevState.stage]: true
          }
        }))
      })
    })
  }

  startTraining = () => {
    this.setState({ stage: 'noSignal', trainingStarted: true })
  }

  playGame = () => {
    if (this.props.advancedMode) {
      this.props.history.push('/game-advanced')
    } else {
      this.props.history.push('/game-basic')
    }
  }

  renderIntroduction = () => (
    <React.Fragment>
      <h1>Treinamento da Inteligência Artificial</h1>

      { this.props.advancedMode ? (
        <React.Fragment>
          <p>
            Ajuste o quadradinho, que está sobre a imagem da sua câmera, para que fique no local onde você vai fazer os sinais em LIBRAS.
            É bem importante que você ajuste o local e o tamanho desse quadradinho para que os sinais se encaixem bem alí dentro.
          </p>
          <p><a target='_blank' rel="noopener noreferrer" href='https://www.youtube.com/watch?v=gmRltDMCB64'>Assistir vídeo de como usar</a></p>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <p>
            Ajuste o quadradinho, que está sobre a imagem da sua câmera, para que fique no local onde você vai colocar o seu brinquedo ou objeto.
            Nós vamos cadastrar primeiro o ambiente, e depois 3 objetos de sua escolha, é importante que os objetos sejam bem diferentes e que eles preencham bem o espaço do quadradinho.
          </p>
          <p><a target='_blank' rel="noopener noreferrer" href='https://www.youtube.com/watch?v=1m4hwU32jdM'>Assistir vídeo de como usar</a></p>
        </React.Fragment>
      ) }

      <Button name='INICIAR O TREINAMENTO' onClick={this.startTraining} />
    </React.Fragment>
  )

  renderFinishedPage = () => (
    <React.Fragment>
      <h1>Treinamento da Inteligência Artificial</h1>
      <h2>Treinamento da inteligência artificial concluído!</h2>
      <Button name='Começar os Exercicios' onClick={this.playGame} />
    </React.Fragment>
  )

  renderSignalLangTraining = (signalLang, onFinish, name, explanation) => (
    <React.Fragment>
      {
        this.props.advancedMode && <h1>Treinamento para "{name || signalLang}"</h1>
      }

      {
        !this.props.advancedMode && <h1>Treinamento para o {({'0': 'primeiro objeto', '1': 'segundo objeto', '2': 'último objeto'})[signalLang] || name || signalLang}</h1>
      }

      {
        signalLang !== 'noSignal' && !this.state.trainingSignal && !this.state.trainedSignals[signalLang] && this.props.advancedMode &&
        <img
          className='training__img'
          src={`${process.env.PUBLIC_URL}/images/training/${signalLang}.png`}
          alt={`Sinal ${signalLang}`} />
      }

      {
        !this.state.trainingSignal && explanation && !this.state.trainedSignals[signalLang] && (
          <p>{explanation}</p>
        )
      }

      {
        !this.state.trainingSignal && !explanation && !this.state.trainedSignals[signalLang] && (
          <p>
            Faça o sinal dentro do quadradinho e clique em "Treinar".
          </p>
        )
      }

      {
        !this.state.trainingSignal && !this.state.trainedSignals[signalLang] && (
          <p>
            Enqunto estiver acontecendo o treinamento, não se mova muito.
          </p>
        )
      }

      {
        this.state.trainingSignal && (
          <div className='training-loading'>
            <Loader />
            <h2>Treinando inteligência artificial... Não se mova!</h2>
          </div>
        )
      }

      {
        !this.state.trainingSignal && !this.state.trainedSignals[signalLang] && (
          <Button name='Treinar' onClick={this.trainSignal} />
        )
      }

      {
        !this.state.trainingSignal && this.state.trainedSignals[signalLang] && (
          <React.Fragment>
            <h2>Treinamento concluído! Clique em "Próximo" para continuar!</h2>
            <Button name='Próximo' onClick={onFinish} />
          </React.Fragment>
        )
      }

    </React.Fragment>
  )

  renderByTrainingStage = () => {
    return this.renderingByStage[this.state.stage]()
  }

  render() {
    return (
      <div className='training default-page'>
        {!this.state.trainingStarted && this.renderIntroduction()}

        {this.state.trainingStarted && !this.state.trainingFinished && this.renderByTrainingStage()}

        {this.state.trainingFinished && this.renderFinishedPage()}
      </div>
    )
  }
}


export default withRouter(Training)
