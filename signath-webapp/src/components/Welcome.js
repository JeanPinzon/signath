import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

import Button from './common/Button'

import '../assets/styles/Welcome.scss'


class Welcome extends Component {

  startAdvanced = () => {
    this.props.history.push('/training-advanced')
  }

  startBasic = () => {
    this.props.history.push('/training-basic')
  }

  render() {
    return (
      <div className='default-page'>
        <h1>Seja bem vind@!</h1>
        <p>
          Somos uma plataforma que utiliza a técnologia para ensinar matemática.
        </p>

        <h2>Aprenda Brincando</h2>
        <p>
          Mais divertido, nesse modo você vai utilizar brinquedos ou objetos para responder os problemas matemáticos.
        </p>
        <Button name='APRENDER BRINCANDO' onClick={this.startBasic} />

        <h2>Modo Libras</h2>
        <p>
          Nesse modo você fará os sinais dos números em Libras para responder os problemas matemáticos.
        </p>
        <Button name='MODO LIBRAS' onClick={this.startAdvanced} />
      </div>
    )
  }
}

export default withRouter(Welcome)
