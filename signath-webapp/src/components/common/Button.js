import React from 'react'

import '../../assets/styles/common/Button.scss'


const Button = ({ name, onClick, className }) => (
  <button
    type="button"
    onClick={onClick}
    className={`button ${className}`}
    name={name}>
    {name}
  </button>
)

export default Button
