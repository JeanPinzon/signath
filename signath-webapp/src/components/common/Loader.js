import React from 'react'

import '../../assets/styles/common/Loader.scss'

const AppLoader = () => (
  <div className='lds-ring'>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
)

export default AppLoader
