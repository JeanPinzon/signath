import React, { Component } from 'react'
import Webcam from "react-webcam"

import '../assets/styles/CameraViewer.scss'


class CameraViewer extends Component {

  webcam = null

  componentDidMount() {
    setInterval(() => {
      this.webcam && this.props.setScreenshotSrc(this.webcam.getScreenshot())
    }, this.props.screenshotIntervalMs);
  }

  render() {
    return (
      <div className='camera-viewer'>
        <Webcam
          audio={false}
          height={this.props.height}
          width={this.props.width}
          ref={(webcam) => { this.webcam = webcam }}
          screenshotFormat="image/jpeg"
        />
      </div>
    )
  }
}

export default CameraViewer
