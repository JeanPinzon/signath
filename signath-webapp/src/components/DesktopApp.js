/* global cv */

import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Cropper from 'react-cropper'

import * as ml5 from 'ml5'

import CameraViewer from './CameraViewer'
import Training from './Training'
import Welcome from './Welcome'
import Game from './Game'
import Loader from './common/Loader'

import 'cropperjs/dist/cropper.css'
import '../assets/styles/DesktopApp.scss'


class DesktopApp extends Component {

  constructor(props) {
    super(props)

    const classifierProps = {
      getNumbersClassifier: this.getNumbersClassifier,
      getFeatureExtractor: this.getFeatureExtractor,
      getMaskedCanvas: this.getMaskedCanvas,
      getMaskedImage: this.getMaskedImage,
      getImageBySignal: this.getImageBySignal,
      setImageForSignal: this.setImageForSignal,
    }

    const classifierPropsAdvanced = {
      ...classifierProps,
      advancedMode: true
    }

    this.trainingAdvancedComponent = () => <Training {...classifierPropsAdvanced} />
    this.gameAdvancedComponent = () => <Game {...classifierPropsAdvanced} />
    this.trainingBasicComponent = () => <Training {...classifierProps} />
    this.gameBasicComponent = () => <Game {...classifierProps} />
  }

  screenshot = null
  maskedCanvas = null
  numbersClassifier = null
  featureExtractor = null

  state = {
    screenshotSrc: null,
    modelLoaded: false,
    cvLoaded: false,
  }

  signalImages = {}

  getImageBySignal = signal => this.signalImages[`${signal}`]

  setImageForSignal = (signal, image) => {
    this.signalImages[`${signal}`] = image
  }

  getNumbersClassifier = () => this.numbersClassifier

  getFeatureExtractor = () => this.featureExtractor

  getMaskedCanvas = () => this.maskedCanvas

  getMaskedImage = () => this.makeFrameMask({ returnImage: true })

  makeFrameMask = ({ returnImage } = {}) => {
    try {
      let img = cv.imread(this.screenshot)

      let result = cv.Mat.zeros(img.rows, img.cols, cv.CV_8UC3)

      const {
        top,
        left,
        height,
        width
      } = this.state.cropData

      result = img.roi(new cv.Rect(left, top, width, height))

      cv.imshow(this.maskedCanvas, result)

      img.delete()

      if (returnImage) {
        return result
      }

      result.delete()
    } catch (e) {
      console.error(e)
    }
  }

  setScreenshotSrc = (screenshotSrc) => {
    this.setState({
      screenshotSrc,
      cropData: this.cropper.cropper.cropBoxData
    }, () => {
      setTimeout(() => this.makeFrameMask(), 100)
    })
  }

  componentDidMount = async () => {
    try {
      this.featureExtractor = ml5.featureExtractor('MobileNet', () => this.setState({ modelLoaded: true }))

      cv['onRuntimeInitialized'] = () => this.setState({ cvLoaded: true })

      this.numbersClassifier = ml5.KNNClassifier()
    } catch (e) {
      console.error('alguma coisa errada não está certa')
      console.error(e)
    }
  }

  render() {
    const { modelLoaded, cvLoaded } = this.state

    if (!modelLoaded || !cvLoaded) {
      return (
        <div className='loader-center'>
          <Loader />
        </div>
      )
    }

    return (
      <div className='desktop-app'>
        <div className='desktop-app__section'>

          <div className='desktop-app__section__main'>
            <img
              src={`${process.env.PUBLIC_URL}/images/logo.png`}
              alt='logo' />

            <CameraViewer
              setScreenshotSrc={this.setScreenshotSrc}
              screenshotIntervalMs={500}
              height={300}
              width={400} />

            <Cropper
              ref={(cropper) => { this.cropper = cropper }}
              src={`${process.env.PUBLIC_URL}/images/transparent.png`}
              className='desktop-app__section__main__crop' />

            <p className='desktop-app__section__main__copyright'>© 2019 Signath | Todos os direitos reservados - <a rel="noopener noreferrer" className='desktop-app__section__main__copyright' target='_blank' href='https://forms.gle/aprFzNaeDZKC64zb8'>Avaliar</a></p>
          </div>

          {
            this.state.screenshotSrc && (
              <React.Fragment>

                <img
                  height={300}
                  width={400}
                  className='hidden'
                  ref={(screenshot) => { this.screenshot = screenshot }}
                  src={this.state.screenshotSrc}
                  alt='hidden screenshot' />

                <canvas
                  height={300}
                  width={400}
                  className='hidden'
                  ref={(maskedCanvas) => { this.maskedCanvas = maskedCanvas }}
                  id='maskedCanvas'/>
              </React.Fragment>
            )
          }

        </div>
        <div className='desktop-app__section light'>
          <BrowserRouter>
            <Switch>
              <Route path='/' component={Welcome} exact />
              <Route path='/training-advanced' component={this.trainingAdvancedComponent} />
              <Route path='/game-advanced' component={this.gameAdvancedComponent} />
              <Route path='/training-basic' component={this.trainingBasicComponent} />
              <Route path='/game-basic' component={this.gameBasicComponent} />
            </Switch>
          </BrowserRouter>
        </div>
      </div>
    )
  }
}

export default DesktopApp
