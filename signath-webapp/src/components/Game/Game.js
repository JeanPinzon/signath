import React, { Component } from 'react'

import GameLesson from './GameLesson'

import '../../assets/styles/Game.scss'

import levelOne from './levels/level-1.json'
import levelTwo from './levels/level-2.json'
import levelThree from './levels/level-3.json'
import levelFour from './levels/level-4.json'


class Game extends Component {

  levels = [
    levelOne,
    levelTwo,
    levelThree,
    levelFour,
  ]

  state = {
    currentLevelIndex: 0,
    currentLessonIndex: 0
  }

  onAnswer = () => {
    setTimeout(() => {
      this.setState((prevState) => {
        const lessonsCount = this.levels[prevState.currentLevelIndex].lessons.length

        if (prevState.currentLessonIndex + 1 >= lessonsCount) {

          if (prevState.currentLevelIndex + 1 >= this.levels.length) {
            return {
              finished: true,
              answerState: 'waiting',
            }
          }

          return {
            currentLevelIndex: prevState.currentLevelIndex + 1,
            currentLessonIndex: 0,
            answerState: 'waiting',
          }
        }

        return {
          currentLessonIndex: prevState.currentLessonIndex + 1,
          answerState: 'waiting',
        }
      })
    }, 6000)
  }

  onAnswerRight = () => {
    this.setState({
      answerState: 'right'
    }, this.onAnswer)
  }

  onAnswerWrong = () => {
    this.setState({
      answerState: 'wrong'
    }, this.onAnswer)
  }

  getClassByAnswerState = () => {
    if (this.state.answerState === 'right') {
      return 'answer-right game default-page'
    }

    if (this.state.answerState === 'wrong') {
      return 'answer-wrong game default-page'
    }

    return 'game default-page'
  }

  render() {
    if (this.state.finished) {
      return (
        <div className="game default-page">
          <div className='game__section'>
            <div className='game__section__header'>
              <h1>Parabéns!</h1>
            </div>
            <div className='game__section__player'>
              <h1>Desafio concluído!</h1>
              <span className='emogis' role='img' aria-label='congrats'>👏 🎉 😎</span>
            </div>
          </div>
        </div>
      )
    }

    return (
      <div className={this.getClassByAnswerState()}>
        <div className='game__section'>

          <div className='game__section__header'>
            <h1>{this.levels[this.state.currentLevelIndex].name}</h1>
          </div>

          <div className='game__section__player'>
            <GameLesson
              lesson={this.levels[this.state.currentLevelIndex].lessons[this.state.currentLessonIndex]}
              level={this.levels[this.state.currentLevelIndex]}
              onAnswerRight={this.onAnswerRight}
              onAnswerWrong={this.onAnswerWrong}
              getNumbersClassifier={this.props.getNumbersClassifier}
              getImageBySignal={this.props.getImageBySignal}
              getFeatureExtractor={this.props.getFeatureExtractor}
              getMaskedCanvas={this.props.getMaskedCanvas}
              advancedMode={this.props.advancedMode} />
          </div>

        </div>
      </div>
    )
  }
}

export default Game
