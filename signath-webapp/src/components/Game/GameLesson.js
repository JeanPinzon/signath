/* global cv */

import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

import Button from '../common/Button'

import '../../assets/styles/GameLesson.scss'


const CLASSIFIERS_RESULTS_LENGHT = 3


const randomInt = () => Math.floor(Math.random() * Math.floor(3))


const signalToPtBR = signal => ({
  "0": " zero ",
  "1": " um ",
  "2": " dois ",
  "3": " três ",
  "4": " quatro ",
  "5": " cinco ",
  "6": " seis ",
  "7": " sete ",
  "8": " oito ",
  "9": " nove ",
  "+": " mais ",
  "-": " menos ",
  "x": " vezes ",
  "÷": " dividido por ",
}[signal])


class GameLesson extends Component {

  buildInitialState = () => ({
    waitingConfirmation: false,
    userResponse: false,
    randomNumber: randomInt(),
    numbersClassifierResults: new Array(CLASSIFIERS_RESULTS_LENGHT).fill('noSignal'),
  })

  state = this.buildInitialState()

  maskedCanvasRel = [null, null, null]

  setMaskedCanvas = () => {
    setTimeout(() => {
      [0, 1, 2].forEach(number => {
        cv.imshow(this.maskedCanvasRel[number], this.props.getImageBySignal(number))
      })
    }, 10)
  }

  setInitialState = () => this.setState(this.buildInitialState())

  buildClassifierFinalResult = (classifierResult) => {
    const results = [...new Set(classifierResult)]

    if (results.length === 1 && results[0] !== 'noSignal') {
      return results[0]
    }

    return false
  }

  validateUserResponse = (state) => {
    if (state.userResponse === this.props.lesson.answer) {
      this.props.onAnswerRight()
    } else {
      this.props.onAnswerWrong(state.userResponse, this.props.lesson.answer)
    }
  }

  buildNumberResult = (state) => {
    return this.buildClassifierFinalResult(state.numbersClassifierResults)
  }

  getResultByClassifier = (classifier, features) => {
    const numLabels = classifier.getNumLabels()

    if (numLabels <= 0) {
      console.error('There is no examples in any label')
      return Promise.reject()
    }

    return new Promise((resolve, reject) => {
      classifier.classify(features, (err, result) => {
        if (err) {
          return reject(err)
        }

        if (result.confidencesByLabel && result.label) {
          return resolve(result.label)
        }
        return reject('not found')
      })
    })
  }

  gotResults = async () => {
    const features = this.props.getFeatureExtractor().infer(this.props.getMaskedCanvas())

    const results = await Promise.all([
      this.getResultByClassifier(this.props.getNumbersClassifier(), features),
    ])

    if (this.state.userResponse && !this.state.waitingConfirmation) {
      return setTimeout(this.gotResults, 1000)
    }

    return this.setState((prevState) => {

      let newState = {}

      if (!prevState.userResponse && !prevState.waitingConfirmation) {

        newState = {
          numbersClassifierResults: this.buildClassifierResult(prevState.numbersClassifierResults, results[0]),
        }

        let userResponse = this.buildNumberResult(newState)

        if (userResponse) {

          if (!this.props.advancedMode) {
            userResponse = this.props.lesson.alternatives[parseInt(userResponse)]
          }

          newState = {
            ...newState,
            userResponse,
            waitingConfirmation: true,
          }
        }

      }

      return newState
    }, () => { setTimeout(this.gotResults, 1000) })
  }

  buildClassifierResult = (classifierResult, result) => {
    classifierResult.push(result)
    return classifierResult.slice(classifierResult.length - CLASSIFIERS_RESULTS_LENGHT)
  }

  renderQuestion = () => {
    const { lesson, level } = this.props

    if (this.state.randomNumber === 0) {
      return (<h1 className='game-lesson__question__title'>{lesson.question[0]} {level.symbol} {lesson.question[1]}</h1>)
    } else if (this.state.randomNumber === 1) {
      return (
        <h1 className='game-lesson__question__title'>
          {signalToPtBR(lesson.question[0])}
          {signalToPtBR(level.symbol)}
          {signalToPtBR(lesson.question[1])}
        </h1>
      )
    }else {
      return (
        <div className='game-lesson__question'>
          <img
            className='game-lesson__question__img'
            src={`${process.env.PUBLIC_URL}/images/training/${lesson.question[0]}.png`}
            alt={`Sinal ${lesson.question[0]}`} />
          <h1 className='game-lesson__question__title'>{level.symbol}</h1>
          <img
            className='game-lesson__question__img'
            src={`${process.env.PUBLIC_URL}/images/training/${lesson.question[1]}.png`}
            alt={`Sinal ${lesson.question[1]}`} />
        </div>
      )
    }
  }

  confirmResponse = () => {
    this.validateUserResponse(this.state)
    this.setState({ waitingConfirmation: false })
  }

  cancelResponse = () => {
    this.setState(this.buildInitialState())
    this.setMaskedCanvas()
  }

  componentDidUpdate(prevProps) {
    try {
      if (prevProps.lesson.question !== this.props.lesson.question) {
        this.setInitialState()
        this.setMaskedCanvas()
      }
    } catch (e) {
      this.props.history.push('/')
    }
  }

  async componentDidMount() {
    try {
      await this.gotResults()
      this.setMaskedCanvas()
    } catch (e) {
      this.props.history.push('/')
    }
  }

  renderAlternatives = (alternatives) => {
    return <div className={ this.props.advancedMode ? 'hidden' : 'user-alternatives' }>
      {[0, 1, 2].map(number => (
        <div key={number} className='user-alternatives__item'>
          <span>{alternatives[number]}</span>
          <canvas
            ref={(maskedCanvas) => { this.maskedCanvasRel[number] = maskedCanvas }}
            id={`maskedCanvas${number}`}/>
        </div>
      ))}
    </div>
  }

  render() {
    const { lesson } = this.props

    if (!this.state.userResponse) {
      return (
        <div className='game-lesson'>
          { this.renderQuestion() }
          { this.props.advancedMode && <h1 className='user-response'>?</h1> }
          { this.renderAlternatives(this.props.lesson.alternatives) }
          { this.props.advancedMode && <h2>Faça o sinal para a resposta no quadradinho que você ajustou sobre a imagem da sua câmera!</h2> }
          { !this.props.advancedMode && <h2>Mostre o objeto da alternativa correta no quadradinho que você ajustou sobre a imagem da sua câmera!</h2> }
        </div>
      )
    }

    if (this.state.userResponse && this.state.waitingConfirmation) {
      return (
        <div className='game-lesson'>
          { this.renderQuestion() }
          <h1 className='user-response'>{this.state.userResponse}</h1>
          <h2>
            Confirma que o resultado é {this.state.userResponse}?
          </h2>
          <div className='game-lesson__btns'>
            <Button className='confirm' name='Confirmar' onClick={this.confirmResponse} />
            <Button className='cancel' name='Cancelar' onClick={this.cancelResponse} />
          </div>
        </div>
      )
    }

    return (
      <div className='game-lesson'>
        { this.renderQuestion() }
        <h1 className='user-response'>{this.state.userResponse}</h1>

        {lesson.answer === this.state.userResponse ? (
          <h2>Resposta certa!</h2>
        ) : (
          <div>
            <h2>Essa foi por pouco! A correta é {lesson.answer}!</h2>
            <h2>Na próxima você acerta!</h2>
          </div>
        )}

        <h2>Indo para o próximo exercício...</h2>
      </div>
    )

  }
}

export default withRouter(GameLesson)
