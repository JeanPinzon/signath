const express = require('express')

const app = express()

app.use(express.static(__dirname + '/signath-webapp/build'))

app.listen(process.env.PORT || 3000)
