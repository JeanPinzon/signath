setup: ## Setup app
	yarn && cd signath-webapp && yarn

run-dev: setup ## Setup and run local app
	cd signath-webapp && yarn start

run-prod: build-prod-statics ## Build and run production app
	yarn start

build-prod-statics: ## Build production statics
	cd signath-webapp && yarn build --prod

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html

help:
	@echo "\n#########################################################################"
	@echo "############################ SIGNATH APP HELP ###########################"
	@echo "#########################################################################\n"

	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

	@echo "\n#########################################################################\n"

.DEFAULT_GOAL := help

.PHONY: setup ,run-dev ,run-prod ,build-prod ,help
